/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function userInfo() {
		let userName = prompt("Enter your fullname: ", "");
		let userAge = prompt("Enter your Age: ", "");
		let userLocation = prompt("Enter your Location: ", "");

		console.log("My fullname is: " + userName);
		console.log("My Age is: " + userAge);
		console.log("My Location is: " + userLocation);
	}
		userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
		function myBand() {
			let faveBand = ["Parokya ni Edgar", 
				"Ben&Ben", 
				"Hale",
				"Autotellic",
				"Munimuni"];

			console.log("Here's the list of my favorite Bands:, ");
			console.log(faveBand);
		}

			myBand();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
		function myMovies() {
		let faveMovies = [{movie: "Claydream", rating: "100%"},
			{movie: "Thor", rating: "66%"},
			{movie: "Minions", rating: "70%"},
			{movie: "Bad Guys", rating: "88%"},
			{movie: "Lost City", rating: "79%"}];

		console.log("Here's the list of my favorite Movies and their Ratings:, ");
		console.log(faveMovies);
	}

		myMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


	let printFriends = function printUsers() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:", ""); 
	let friend2 = prompt("Enter your second friend's name:", ""); 
	let friend3 = prompt("Enter your third friend's name:", "");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();




